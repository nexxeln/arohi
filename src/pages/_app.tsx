import type { AppType } from "next/app";
import { Karla } from "@next/font/google";
import "../styles/globals.css";

const karla = Karla({ subsets: ["latin"], display: "swap" });

const MyApp: AppType = ({ Component, pageProps }) => {
  return (
    <div className={karla.className}>
      <Component {...pageProps} />
    </div>
  );
};

export default MyApp;
