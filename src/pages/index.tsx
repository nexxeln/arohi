import { type NextPage } from "next";
import Head from "next/head";
import useWindowSize from "../hooks/useWindowSize";
import Confetti from "react-confetti";
import { useEffect, useState } from "react";

const Home: NextPage = () => {
  const { width, height } = useWindowSize();
  const [isConfetti, setIsConfetti] = useState(false);

  const getAge = () => {
    const birth = Date.parse(new Date("28 November 2005 15:02 GMT").toString());

    const msSinceBirth = Date.parse(Date()) - birth;

    const age = (msSinceBirth / 1000 / 60 / 60 / 24 / 366).toPrecision(9);
    return age;
  };

  const [age, setAge] = useState(getAge());

  // reset age every 1 second
  useEffect(() => {
    const interval = setInterval(() => {
      setAge(getAge());
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <>
      <Head>
        <title>arohi</title>
        <meta
          name="description"
          content="happy birthdayyyyy b swear worddddd"
        />
        <link rel="icon" href="https://fmj.asrvd.me/≡ƒÆ£" />
      </Head>
      <main className="mx-auto flex h-screen w-11/12 max-w-[75rem] flex-col justify-center md:w-8/12 lg:w-7/12 xl:w-5/12">
        <h1 className="pb-0.5 text-lg font-semibold text-white">
          Happy Birthday Arohi
        </h1>
        <p>
          Hi Arohiiiiii. I can't believe you're like {age} now damn. Same age as
          me. I hope you have a great birthday and maybe this website will make
          it even better :)
        </p>
        <p>
          I think we've been talking to each other for like 3 years now? Almost?
          I've known you for even more! I had never even imagined I would talk
          to you ever, and here we are. I never thought that we'd talk so much
          that we'd run out of questions to ask in tnt. I never thought you
          would become the person I go to for advice. So many nevers. But you
          made it possible. Thank you for being such a special part of my life.
          You're like the only person I talk to regularly and it makes me happy.
          I know we've grown apart quite a bit but I hope we will be friends
          till I'm old.{" "}
          <a
            href="https://this-vegetable.is-from.space/Discord_TW4u9FOauO.png"
            target="_blank"
          >
            Right?
          </a>
        </p>

        <p>
          I truly appreciate every second of your time that you've spent with
          me. It means a lot, especially when everyone else was leaving. Now I
          think this letter of whatever is becoming a bit too formal so yes fuck
          you b swear word.
        </p>

        <p>Love you {"<3"}</p>

        <p>
          <button
            className={`${
              isConfetti ? "cursor-normal" : "cursor-pointer underline"
            }`}
            disabled={isConfetti}
            onClick={() => setIsConfetti((prev) => !prev)}
          >
            {isConfetti
              ? " Wooohooo happy birthday arohiiiiii"
              : "Click this by the way."}
          </button>
        </p>

        <Confetti width={width} height={height} run={isConfetti} />
      </main>
    </>
  );
};

export default Home;
